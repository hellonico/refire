(ns refire.core
    (:require [pani.cljs.core :as p]
              [figwheel.client :as fw]
              [cljs.core.async :as async :refer [put! <! >! chan]]
              [reagent.core :as reagent :refer [atom]]
              [reagent.session :as session]
              [secretary.core :as secretary :include-macros true]
              [goog.events :as events]
              [goog.history.EventType :as EventType])
    (:require-macros [cljs.core.async.macros :refer [go]])
    (:import goog.History))

;; -------------------------
;; Views

(enable-console-print!)
;(println "hello2")
;(defn log [& more]
; (.apply (.-log js/console) js/console
;         (into-array (map #(if (satisfies? cljs.core.ISeqable %)
;                             (pr-str %)
;                             %)
;                          more))))
;(log "Hello")

(defn home-page []
  [:div [:h2 "Welcome to refire"]
   [:div [:a {:href "#/about"} "go to about page"]]])

(defn about-page []
  [:div [:h2 "About refire"]
   [:div [:span (str (session/get :firebase))]]
   [:div 
   [:table.u-full-width
    [:thead [:tr [:td "Name"]]]
    [:tbody ]

   ]]
   [:div [:a {:href "#/"} "go to the home page _"]]])

(defn current-page []
  [:div [(session/get :current-page)]])

;; -------------------------
;; Routes
(secretary/set-config! :prefix "#")

(secretary/defroute "/" []
  (session/put! :current-page #'home-page))

(secretary/defroute "/about" []
  (session/put! :current-page #'about-page))

;; -------------------------
;; History
;; must be called after routes have been defined
(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

;; -------------------------
;; Initialize app
(defn init! []
  (hook-browser-navigation!)
  (reagent/render-component [current-page] (.getElementById js/document "app")))

;
; Firebase
;
(def r 
  (p/root "https://blistering-inferno-558.firebaseio.com/"))
(p/bind r 
  :child_added 
  :messages #(println (str %)))

; get notification
(p/bind r 
  :value 
  :size #(session/put! :firebase (str %)))

(def previous (p/get-in r [:info :world]))
(go (let [v (<! previous)]
 (println (str v))))

(def today (js/Date))
; update a database value
(p/set! r [:info :world] (str today))


(defn get-position []
  (let [out (chan)
        geo (.-geolocation js/navigator)]
  (.getCurrentPosition geo (fn [pos] (put! out pos)))
  out))
 
(go
  (let [coords (.-coords (<! (get-position)))
        latitude (.-latitude coords)
        longitude (.-longitude coords)]
        (p/set! r [:info :loc] (str "Lat:" latitude "Long:" longitude))))
